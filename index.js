import express, { json } from "express";
import { traineesRouter } from "./src/route/traineesRouter.js";
import { firstRouter } from "./src/route/firstRouter.js";
import { nameRouter } from "./src/route/nameRouter.js";
import { bikeRouter } from "./src/route/bikeRouter.js";
import connectToMongoDb from "./src/connectdb/connectToMongoDb.js";
let expressApp = express();
connectToMongoDb();

expressApp.use((req,res,next)=>{
  console.log("I am application, normal middleware1")
  let error= new Error("i am application error");
  next(error)
},
(error, req,res,next)=>{
  console.log("I am application, error middleware1")
  console.log(error.message);
  next();
},
(req,res,next)=>{
  console.log("I am application, normal middleware2")
  next();
})


expressApp.use(json());  //it is done to make our application to accept json data

expressApp.use("/trainees", traineesRouter);
expressApp.use("/", firstRouter);
expressApp.use("/name", nameRouter);
expressApp.use("/bike", bikeRouter);

expressApp.listen(8000, () => {
  console.log("app is listening at port 8000");
});
