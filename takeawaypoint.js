/* 
*********CRUD***********
create => post
read => get
update => patch
delete => delete
CRUD is always related to database
Frontend request backend
Backend gives response frontend
*/

/* 
url=localhost:8000
method=post
*/

/* 
always place dynamic route at the end
we can send data from postman by three way body, params, query
what ever sent in url will come in string
one request must have one response
*/

////url
/* 
localhost:8000/a/b?name=nitan&age=29&address=gagalphedi
url = route + query
route =localhost:8000/a/b
route = baseUrl+ route params
baseUrl = localhost:8000
query =name=nitan&age=29&address=gagalphedi
*/

/* 
middleware
   middleware are the function which has req, res,next
   next is used to trigger another middleware
   we have two form of middleware
            - error middleware (err,req,res, next)=>{}
                to trigger error middleware we have to call next(value)
            - normal middleware(req,res, next)=>{}
                to trigger normal middleware we have to call next() 

  middleware is divided into parts
     route middleware
     application middleware
 */

